
# Instalação do banco de dados


# Instalação do PHP

## Pré-requisitos

### Cent OS/Red het
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
yum install xinetd\
            compat-libstdc++-33 \
            autoconf \
            make \
            gcc \
            gcc-c++ \
            bison \
            openssl-devel \
            libxml2 \
            libxml2-devel \
            libjpeg-devel \
            flex \
            httpd \
            httpd-devel \
            bzip2-devel \
            gmp-devel \
            libpng-devel \
            libXpm-devel \
            freetype-devel \
            fontconfig-devel \
            libmcrypt libmcrypt-devel \
            gd-devel \
            mod_ssl \
            libtool-ltdl \
            libtool-ltdl-devel \
            expat-devel \
            cups-lpd\
            expat.x86_64 \
            compat-expat1.x86_64 \
            expat-devel.x86_64\
            libXpm\
            nfs\
            nfs-utils\
            cups-pdf\
            pdf-tools\
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
##### Configurando ldconfig para o cent os
~~~~
echo "/usr/local/lib" > /etc/ld.so.conf.d/zelus.conf
~~~~


### Ubuntu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
apt install \
    apache2 \
    xinetd \
    autoconf \
    make \
    libstdc++-5-dev \
    apache2-dev \
    libzip-dev \
    bison \
    libxpm-dev \
    libpng++-dev \
    flex \
    libbz2-dev \
    libxml2-dev \
    libgd-dev \
    libgmp-dev \
    libjpeg-dev \
    libstdc++5 \
    libtool-bin \
    libtiff-dev 

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


#### Instalando a versão 2.2 (Ubuntu)
~~~~ 

  738  wget http://archive.apache.org/dist/httpd/httpd-2.2.29.tar.gz
  742  apt-mark hold apache2
  755  tar zxfv httpd-2.2.29.tar.gz 
  756  cd httpd-2.2.29/
  840  ./configure   --enable-rewrite --enable-ssl --enable-alias
  841  make
  842  make install


~~~~



## Compilando o PHP

### pré-requisitos
~~~~
mkdir /usr/src/zelus
~~~~
Copie o conteúdo do sources do pacote para o diretório `/usr/src/zelus/souces`


#### Configurando libraris
~~~~
cd /usr/lib
ln -s ../lib64/libpng* .
ln -s ../lib64/libjpeg.so* .
ln -s ../lib64/libXpm.so* .
ln -s /usr/include /opt/include
ln -s /usr/lib64 /opt/lib
ln -s ../lib64/libpng* .
ln -s ../lib64/libjpeg.so* .
ln -s ../lib64/libXpm.so* .
~~~~


#### Instalar o Firebird para compilar o php
~~~~
cd /usr/src/zelus/
tar zxfv sources/FirebirdCS-2.0.7.13318-0.amd64.tar.gz
cd FirebirdCS-2.0.7.13318-0.amd64/
./install.sh

cp firebird/confs/firebird/x64/conf/firebird.conf /opt/firebird/
cp firebird/confs/firebird/x64/lib64/rfunc /opt/firebird/UDF/
chown firebird: -Rf /opt/firebird/UDF
cp firebird/confs/firebird/x64/etc/xinetd.d/firebird /etc/xinetd.d
~~~~


#### libMCrypt
~~~~
cd /usr/src/zelus/
tar zxfv sources/libmcrypt-2.5.8.tar.gz
cd libmcrypt-2.5.8/
./configure --prefix=/usr/local
make && make install
ldconfig -v
~~~~

#### mHash
~~~~
cd /usr/src/zelus/
tar jxfv sources/mhash-0.9.9.9.tar.bz2
cd mhash-0.9.9.9/
./configure --prefix=/usr/local
make && make install
ldconfig -v
~~~~


#### mcrypt
~~~~
tar zxfv sources/mcrypt-2.6.8.tar.gz
cd mcrypt-2.6.8/
./configure --prefix=/usr/local
make && make test && make install
~~~~


#### Openssl para o (Somente para CentOS)
~~~~
 ./Configure linux-generic64 --prefix=/usr/local
 make
 make install
 ldconfig -v


 cd openssl-0.9.8zg/
 ./Configure linux-generic64
 make
 make install
 
~~~~



#### PHP
~~~~
cd /usr/src/zelus/
tar jxfv sources/php-4.4.0.tar.bz2 
cd php-4.4.0





./configure \
    --prefix=/usr/php/4.4.0 \
    --cache-file=../config.cache \
    --with-config-file-path=/etc \
    --with-config-file-scan-dir=/etc/php.d  \
    --enable-force-cgi-redirect \
    --disable-debug \
    --enable-pic \
    --enable-inline-optimization \
    --with-bz2  \
    --with-dom \
    --with-freetype \
    --with-png  \
    --with-gd \
    --enable-gd-native-ttf \
    --with-ttf \
    --with-gettext \
    --with-iconv \
    --with-jpeg \
    --with-png \
    --with-zlib  \
    --with-layout=GNU \
    --enable-bcmath \
    --enable-exif \
    --enable-ftp \
    --enable-magic-quotes \
    --enable-safe-mode  \
    --enable-sockets \
    --enable-sysvsem \
    --enable-sysvshm \
    --enable-track-vars \
    --enable-trans-sid \
    --enable-yp  \
    --enable-wddx  \
    --with-pear\
    --with-kerbeross \
    --with-interbase \
    --enable-ucd-snmp-hack \
    --enable-memory-limit \
    --enable-versioning \
    --with-apxs2=/usr/bin/apxs2 \
    --with-xpm \
    --with-pcre-regex \
    --with-mcrypt=/usr/local/bin/mcrypt \
    --enable-pcntl \
    --with-jpeg-dir=/opt \
    --with-png-dir=/opt 

~~~~

## Configurando o Apache


#### Habilitando os módulos do apache

##### Debian
~~~~
a2enmod alias rewrite
~~~~

##### CentOs
~~~~
?
~~~~